import logging
import os

from dotenv import load_dotenv
from telegram.ext import DictPersistence

from money_observer.utils import get_allowed_users_as_list

load_dotenv()


def configure_logging(level='INFO'):
    format_ = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=level, format=format_)


if __name__ == '__main__':
    from money_observer.spread_sheet_bean import SpreadSheetBean
    from money_observer.telegram_bot import TelegramBot
    from money_observer.scheduler import Scheduler

    configure_logging()
    logger = logging.getLogger(__name__)

    logger.info('App start')

    ssb = SpreadSheetBean(
        google_cert_file_name='secret/money-time.json',
        sheet_information={
            'title': '08.03.2021 start amount',
            'email': 'vkvc916@gmail.com',
        },
        budget=int(os.getenv('BUDGET')),
    )

    with open('secret/telegram_token.txt', 'r') as token_file:
        token = token_file.read()

    persistence = DictPersistence()
    tb = TelegramBot(
        token=token,
        spread_sheet_bean=ssb,
        persistence=persistence,
        allowed_user=get_allowed_users_as_list(os.getenv('ALLOWED_USERS')),
    )

    sh = Scheduler(ssb, tb)
    sh.start_scheduler()

    try:
        tb.start_bot()
    except Exception as e:
        logger.exception(f'Bot error: {e}')

    logger.info(f'Persistence: {persistence.user_data}')
    logger.info('App finish')
