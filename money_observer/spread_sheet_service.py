import logging
from typing import Optional, List

import gspread
from gspread import Spreadsheet, Client, SpreadsheetNotFound

logger = logging.getLogger(__name__)


class SpreadSheetService:
    DEFAULT_SCOPES = [
        'https://www.googleapis.com/auth/spreadsheets',
        'https://www.googleapis.com/auth/drive',
    ]
    # TODO: create values for permission, get from disk not request

    def __init__(
            self,
            file_name: str,
            scopes: Optional[List[str]] = None):
        self._scopes: List[str] = scopes if scopes else self.DEFAULT_SCOPES
        self._google_client: Client = get_service_account(file_name, self._scopes)

    def create_spreadsheet(
            self,
            title: str,
            folder_id: Optional[str] = None
    ) -> Spreadsheet:
        sh = self._google_client.create(title, folder_id)
        logger.info(f'Sheet {title} created {sh.url} {sh.id}')
        return sh

    def open_spreadsheet(
            self,
            title: Optional[str] = None,
            key: Optional[str] = None,
            url: Optional[str] = None,
            **kwargs,
    ) -> Optional[Spreadsheet]:
        sh = None
        try:
            if title:
                sh = self._google_client.open(title)
            if key:
                sh = self._google_client.open_by_key(key)
            if url:
                sh = self._google_client.open_by_url(url)
        except SpreadsheetNotFound:
            pass

        if sh:
            logger.info(f'Spreadsheet loaded {sh.title} {sh.url} {sh.id}')
        else:
            logger.error('Spreadsheet not loaded')
        return sh


def get_service_account(file_name: str, scope: Optional[List[str]] = None) -> Client:
    """
    :param file_name: file with project credential https://console.developers.google.com/apis/credentials
    :param scope: available google api https://console.developers.google.com/apis/
    :return: google client
    """
    return gspread.service_account(file_name, scope)


def share_spread_sheet_to_user(
        spread_sheet: Spreadsheet,
        user_email: str,
        rule: str,
        perm_type: str = 'user',
        **kwargs,
) -> None:
    spread_sheet.share(user_email, perm_type=perm_type, role=rule, **kwargs)

