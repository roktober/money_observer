import logging

from apscheduler.schedulers.background import BackgroundScheduler

from money_observer.base_format_processor import TZ
from money_observer.spread_sheet_bean import SpreadSheetBean
from money_observer.telegram_bot import TelegramBot

logger = logging.getLogger(__name__)


class Scheduler:
    def __init__(
        self,
        spread_sheet_bean: SpreadSheetBean,
        telegram_bot: TelegramBot,
    ):
        self._scheduler = BackgroundScheduler()
        self._spread_sheet_bean = spread_sheet_bean
        self._telegram_bot = telegram_bot

    def every_day_stat(self) -> None:
        day_stat = self._spread_sheet_bean.day_stat()
        for chat_id in self._telegram_bot.persistence.get_user_data().keys():
            self._telegram_bot.send_message(chat_id, day_stat)

    def start_scheduler(self) -> None:
        self._scheduler.add_job(self.every_day_stat, 'cron', hour=23, minute=55, timezone=TZ)
        self._scheduler.start()
