"""
Base sheet format
A - time
B - reason
C - amount of money
Base msg format
\n - separator, effect only first separator, see maxsplit
amount of money\n reason
"""
from datetime import datetime
import os

import pytz
import tzlocal

from pandas import DataFrame

BASE_DATAFRAME_COLUMNS = ['time', 'reason', 'amount']
DATE_FORMAT = '%Y-%m-%d %H:%M:%S'
TZ = pytz.timezone(os.getenv('TZ') or str(tzlocal.get_localzone()))


def get_data_from_msg(msg: str) -> DataFrame:
    amount_reason = msg.split('\n', maxsplit=1)
    if len(amount_reason) != 2:
        raise ValueError(f'Invalid message {msg}')
    amount = float(amount_reason[0])
    reason = amount_reason[1]
    data = DataFrame(
        data=
        [
            [
                datetime.now(tz=TZ).strftime(DATE_FORMAT),
                reason,
                amount
            ]
        ],
        columns=BASE_DATAFRAME_COLUMNS,
    )
    return data

