from io import StringIO
from typing import List

import pandas as pd
from pandas import DataFrame

from money_observer.base_format_processor import TZ, DATE_FORMAT


def get_dataframe_info_as_string(df: DataFrame, verbose=True, **kwargs) -> str:
    buffer = StringIO()
    df.info(buf=buffer, verbose=verbose, **kwargs)
    return buffer.getvalue()


def str_datetime_to_datetime_dataframe(df: DataFrame) -> None:
    df['time'] = pd.to_datetime(df['time'], format=DATE_FORMAT)
    df['time'] = df.apply(lambda time: time['time'].tz_localize(TZ), axis=1)


def get_allowed_users_as_list(allowed_users: str) -> List[int]:
    return [int(user) for user in allowed_users.split(',')]
