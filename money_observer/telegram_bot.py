import logging
from functools import wraps
from typing import List, Callable

from telegram import Update
from telegram.ext import Updater, CallbackContext, CommandHandler, MessageHandler, Filters, BasePersistence

from money_observer.spread_sheet_bean import SpreadSheetBean

logger = logging.getLogger(__name__)


class TelegramBot:
    def __init__(
            self,
            token: str,
            spread_sheet_bean: SpreadSheetBean,
            persistence: BasePersistence,
            allowed_user: List[int],
    ):
        self.updater = Updater(token, persistence=persistence)
        self.dispatcher = self.updater.dispatcher
        self.spread_sheet_bean = spread_sheet_bean
        self.persistence = persistence
        self.allowed_user = allowed_user

    def start_bot(self):
        self.dispatcher.add_handler(CommandHandler("start", _start_command))
        self.dispatcher.add_handler(CommandHandler("help", _help_command))

        self.dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, self._observe))

        self.dispatcher.add_handler(CommandHandler("day", self._day_amount_command))
        self.dispatcher.add_handler(CommandHandler("week", self._week_amount_command))
        self.dispatcher.add_handler(CommandHandler("url", self._url_command))
        self.dispatcher.add_handler(CommandHandler("day_stat", self._day_stat_command))

        self.updater.start_polling()

        # Run the bot until you press Ctrl-C or the process receives SIGINT,
        # SIGTERM or SIGABRT. This should be used most of the time, since
        # start_polling() is non-blocking and will stop the bot gracefully.
        self.updater.idle()

    def send_message(self, chat_id: int, text: str, **kwargs):
        self.dispatcher.bot.send_message(chat_id, text, **kwargs)
        logger.info(f'Message sent to {chat_id} msg: {text}')

    def allow_act(f: Callable):
        @wraps(f)
        def wrapper(*args, **kwargs):
            self: 'TelegramBot' = args[0]
            update: Update = args[1]
            callback: CallbackContext = args[2]
            if update.effective_chat.id in self.allowed_user:
                f(*args, **kwargs)
            else:
                logger.info(f'Access denied for user: {update.message.from_user}')
        return wrapper

    @allow_act
    def _observe(self, update: Update, context: CallbackContext) -> None:
        msg_text = update.message.text
        logger.info(f'New message {msg_text} from {update.message.from_user}')
        stat_data = self.spread_sheet_bean.observ(msg_text, str(update.message.from_user))
        update.message.reply_text(stat_data)


    @allow_act
    def _day_amount_command(self, update: Update, context: CallbackContext) -> None:
        day_amount = self.spread_sheet_bean.day_amount()
        logger.info(f'Day amount command from {update.message.from_user} amount: {day_amount}')
        update.message.reply_text(f'Day amount {day_amount}')

    @allow_act
    def _week_amount_command(self, update: Update, context: CallbackContext) -> None:
        week_amount = self.spread_sheet_bean.week_amount()
        logger.info(f'Week amount command from {update.message.from_user} amount: {week_amount}')
        update.message.reply_text(f'Week amount {week_amount}')

    @allow_act
    def _url_command(self, update: Update, context: CallbackContext) -> None:
        url = self.spread_sheet_bean.spread_sheet.url
        logger.info(f'Url command from {update.message.from_user} url: {url}')
        update.message.reply_text(f'You url: {url}')

    @allow_act
    def _day_stat_command(self, update: Update, context: CallbackContext) -> None:
        day_stat = self.spread_sheet_bean.day_stat()
        logger.info(f'Day stat command from {update.message.from_user} day stat: {day_stat}')
        update.message.reply_text(day_stat)


def _start_command(update: Update, context: CallbackContext) -> None:
    logger.info(f'Start command from {update.message.from_user}')
    update.message.reply_text('Hi!')


def _help_command(update: Update, context: CallbackContext) -> None:
    logger.info(f'Help command from {update.message.from_user}')
    update.message.reply_text('Help!')
