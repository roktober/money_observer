import logging
from typing import Optional

import pandas as pd

from gspread import Spreadsheet, Worksheet
from pandas import DataFrame

from money_observer.utils import get_dataframe_info_as_string

logger = logging.getLogger(__name__)


class SpreadSheetProcessor:
    def __init__(
            self,
            spread_sheet: Spreadsheet,
            sheet_index: Optional[int] = 0,
    ):
        self.spread_sheet = spread_sheet
        self.worksheet: Optional[Worksheet] = spread_sheet.get_worksheet(sheet_index)
        if not self.worksheet:
            raise ValueError(f'Worksheet with index {sheet_index} not found in spreadsheet {self.spread_sheet!r}')

        self.dataframe: DataFrame = pd.DataFrame(self.worksheet.get_all_records())

        logger.info(f'Data loaded {get_dataframe_info_as_string(self.dataframe)}')

    def update_worksheet(self):
        # TODO: more effective update only new values
        self.worksheet.update([self.dataframe.columns.values.tolist()] + self.dataframe.values.tolist())

    def add_data(self, data: DataFrame):
        if len(data.columns) != len(self.dataframe.columns) and len(self.dataframe.columns) != 0:
            raise ValueError(f'Incorrect new data format, '
                             f'has {len(data.columns)} columns vs {len(self.dataframe.columns)} '
                             f'in original dataframe')

        if len(self.dataframe.columns) == 0:
            self.dataframe = data
        else:
            self.dataframe = self.dataframe.append(data, ignore_index=True)
        logger.info(f'Data appended {get_dataframe_info_as_string(data)}')

    def get_dataframe(self):
        """
        :return: copy of original dataframe
        """
        return self.dataframe.copy()
