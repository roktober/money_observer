import logging
from typing import Optional, List, Dict
from datetime import datetime
import calendar
from gspread import Spreadsheet
from pandas import DataFrame

from money_observer.base_format_processor import get_data_from_msg, TZ
from money_observer.spread_sheet_processor import SpreadSheetProcessor
from money_observer.spread_sheet_service import SpreadSheetService, share_spread_sheet_to_user
from money_observer.utils import str_datetime_to_datetime_dataframe

logger = logging.getLogger(__name__)


class SpreadSheetBean:
    def __init__(
            self,
            google_cert_file_name: str,
            sheet_information: Dict[str, str],
            # key can be: title, key, url, user email see SpreadSheetService.open_spreadsheet
            # TODO replace to dataclass
            budget: int,
            scopes: Optional[List[str]] = None,
    ):
        self.budget = budget

        self.spread_sheet_service = SpreadSheetService(google_cert_file_name, scopes)

        sheet: Spreadsheet = self.spread_sheet_service.open_spreadsheet(**sheet_information)
        if sheet:
            self.spread_sheet = sheet
        else:
            email: Optional[str] = sheet_information.get('email', None)
            title: Optional[str] = sheet_information.get('title', None)

            if not email or not title:
                raise ValueError(f'Spreadsheet cant be created with {sheet_information}')

            self.spread_sheet = self.spread_sheet_service.create_spreadsheet(title)
            share_spread_sheet_to_user(self.spread_sheet, email, 'owner')

        self.spread_sheet_processor = SpreadSheetProcessor(self.spread_sheet)

    def observ(
            self,
            msg: str,
            user: str,
    ) -> str:
        try:
            data = get_data_from_msg(msg)
            self.spread_sheet_processor.add_data(data)
            self.spread_sheet_processor.update_worksheet()
        except Exception as e:
            logger.exception(f'Cant parse message {msg} from user {user}: {e}')
            return 'Cant process message'
        now = datetime.now(TZ)
        days_in_month = calendar.monthrange(now.year, now.month)[1]
        day_amount = self.day_amount()
        budget_per_day = int(self.budget / days_in_month)
        week_amount = self.week_amount()
        budget_per_week = int(self.budget / days_in_month * 7)
        msg_day_amount = f'Сегодня потрачено: {day_amount}'
        msg_day = f'Осалось на день: {budget_per_day - day_amount}'
        msg_week = f'Осталось на неделю: {budget_per_week - week_amount}'
        return f'{msg_day_amount}\n{msg_day}\n{msg_week}'

    def day_amount(self) -> float:
        table_data = self.spread_sheet_processor.get_dataframe()
        if not table_data.values.any():
            return 0

        now = datetime.now(tz=TZ)
        str_datetime_to_datetime_dataframe(table_data)
        today_data = table_data[table_data['time'].dt.day.eq(now.day)]
        return today_data['amount'].sum()

    def week_amount(self) -> float:
        table_data = self.spread_sheet_processor.get_dataframe()
        if not table_data.values.any():
            return 0

        now = datetime.now(tz=TZ)
        str_datetime_to_datetime_dataframe(table_data)
        today_data = table_data[table_data['time'].dt.week.eq(now.isocalendar()[1])]
        return today_data['amount'].sum()

    def avg_day_current_week(self) -> float:
        week_amount = self.week_amount()
        now = datetime.now(tz=TZ)
        avg_current_week = week_amount / now.isocalendar()[2]
        return round(avg_current_week)

    def day_stat(self) -> str:
        day_amount = self.day_amount()
        week_amount = self.week_amount()
        avg_day_current_week = self.avg_day_current_week()
        return f'Траты сегодня: {day_amount}\nТраты за эту неделю: {week_amount}\nВ среднем в день на этой неделе: {avg_day_current_week}'